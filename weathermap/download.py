#!/usr/bin/env python3
# download the current rrdtool images from the weathermap
import requests
from io import BytesIO
import functools

def retry(retries):
	def retry_decorator(func):
		@functools.wraps(func)
		def retry_wrapper(*args, **kwargs):
			counter = 0
			while counter <= retries:
				try:
					return func(*args, **kwargs)
				except requests.exceptions.HTTPError:
					counter += 1
				except requests.exceptions.ConnectionError:
					break
			raise DownloaderException("Can't download {}".format(args[1]))
		return retry_wrapper
	return retry_decorator

class DownloaderException(Exception):
	pass

class Downloader:
	CONTACT = "Contact: undefined"

	def __init__(self, generator):
		self.session = requests.Session()
		self._update_ua(generator)

	def _update_ua(self, ua):
		ua = {"User-Agent": "{}; {}".format(ua, self.CONTACT)}
		self.session.headers.update(ua)

	@retry(3)
	def get(self, url):
		buffer = BytesIO()
		request = self.session.get(url)
		request.raise_for_status()
		for chunk in request.iter_content(chunk_size=1024):
			buffer.write(chunk)
		return buffer

	@retry(1)
	def head(self, url):
		request = self.session.head(url)
		request.raise_for_status()
		return request.headers
