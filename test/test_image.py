import unittest
from weathermap.image import Image as WImage
from PIL import Image as PImage

class ImageTest(unittest.TestCase):
	image = "test/data/xwin-dfn-fra.png"
	image_thr = "test/data/xwin-dfn-fra_threshold.png"
	image_res = "test/data/xwin-dfn-fra_resize.png"
	image_pro = "test/data/xwin-dfn-fra_process.png"

	def setUp(self):
		self.wimage = WImage(self.image, self.image)
		self.pimage = PImage.open(self.image)

		self.i_thr = PImage.open(self.image_thr)
		self.i_res = PImage.open(self.image_res)
		self.i_pro = PImage.open(self.image_pro)

	def tearDown(self):
		pass

	def test_threshold(self):
		self.wimage.threshold()
		self.assertEqual(self.wimage.get_image().tobytes(), self.i_thr.tobytes())

	def test_resize(self):
		self.wimage.threshold()
		self.wimage.resize()
		self.assertEqual(self.wimage.get_image().tobytes(), self.i_res.tobytes())

	def test_process(self):
		self.wimage.threshold()
		self.wimage.resize()
		self.wimage.sharpen()
		self.assertEqual(self.wimage.get_image().tobytes(), self.i_pro.tobytes())

	def test_filename(self):
		self.assertEqual(self.wimage.image.filename, self.image)
