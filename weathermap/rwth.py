from .weathermap import Weathermap
import time

class RWTH(Weathermap):
	BASEURL = "https://netstatus.rz.rwth-aachen.de/status/weathermap/"
	IMGURL = {
		"xwin-credo": "04a4b84ec557ca1625bfd707924e3791.png",
		"xwin-fzj": "b57c8e257dca1cd524643ad26174cc3b.png",
		"xwin-netcologne": "0a241770823802decf543cd015921d28.png",
		"xwin-relaix": "2cbe2a4e7fc24fa69fa45f9edcf2e43d.png",
		"xwin-dfn-fra": "43307e0f114a64b96560316c902d6f00.png",
		"xwin-dfn-han": "843441435f17800e5fc48f19eb39f3b9.png",
		"xwin-physik-grid-0": "cec0eb12b2d896ad2e9af3d33d782087.png",
		"xwin-physik-grid-1": "d3e284d22d3591af36f0154217ae3e25.png",
		"xwin-fh": "872c914a103332dc4fed0ad5913785b6.png",
		"xwin-fw-1": "e986ac4792ce713c9c476b90f5a0ce36.png",
		"xwin-fw-2": "4ef2f917da00fc4984ad1e7ddea933e5.png",
		"sc-fw-1": "05ceb621546d8855f5186fd788866146.png",
		"sc-fw-2": "35830e402bc4849d593e1f81ebe04e62.png",
		"sc-hg-leg-0": "58114c84c8b9861b1479e56356a49b1f.png",
		"sc-hg-leg-1": "a0cf292b79e5c2bd409551c69251d55d.png",
		"sc-sw23-leg-0": "e3b99b50687ddbf054c50c94cade1ba3.png",
		"sc-sw23-leg-1": "98b1b4e27445d6a641cf542c81223e34.png",
		"sc-ww10-0": "73a9a6463f8a85fa23e5dddc3f102f0e.png",
		"sc-ww10-1": "13c4e25d3864d60c2ed5fa099cb93f57.png",
		"sc-backup-0": "afbc0339e890be927307e739f10b709b.png",
		"sc-backup-1": "ecc808e3527feb506935e3f00d52187d.png",
		"sc-backup-2": "03969347d97807f99066b0dccfd6c536.png",
		"sc-backup-3": "013e56cab42b5704884e640b89d48091.png",
		"sc-hpc-0": "aef4aa0d4863df65ed8d7668006baab6.png",
		"sc-hpc-1": "64d02e15e6237aa4df4b9cace62891ce.png",
		"sc-dc-0": "80542a7d99cd0decb7a4a29ac848617e.png",
		"sc-dc-1": "a5ccd719907e94e9a9ce1915de88cb44.png",
		"sc-dc-2": "bf8ba8c2bef5e761a28a63746ef34b77.png",
		"sc-dc-3": "8d9ba29a96650bd47360751092456473.png",
		"sc-noc-0": "b94798c57ce455ba4eda0a3da4d8d81f.png",
		"sc-noc-1": "949b0d762b981cfcb6e4b86400d9c9ec.png",
		"sc-rog-leg-0": "9424b7ff7ed5ecf3477145e0e7d7ab7e.png",
		"sc-rog-leg-1": "927029b2dd3bdccfe67f0cc29af8d541.png",
		"ww10-physik": "4e2edfe10357c58e9b1b9b367177bc3c.png",
		"lss-physik": "b279a9abb0d38b3bc4f1aea01840ae29.png",
		"carl-zhv": "5af8557fb04d58420aeaca9a1082da36.png",
		"sw23-zhv": "2c00252e9f8715d1455fd4e88da9b57b.png",
		"ww10-vpn": "411460a76181e74dda85b02f78cb4d2f.png",
		"sc-wlan-0": "fd570b3bd61d4fbbe199ed54ff8ff9ec.png",
		"sc-wlan-1": "653fd35843041b2e10febf81a1a4ccaf.png",
		"sc-wh-0": "20c2d27d3d29a214e845c6cbe440b849.png",
		"sc-wh-1": "2a044ef0693963a7cd676b19182857fc.png",
		"sc-carl-0": "1e22154ba03da896548c6213f007787c.png",
		"sc-carl-1": "501f2ded932599423d67653bfa4cf7f5.png",
		"dc-otto-2-0": "78263504fce4c02def3c26b486cca929.png",
		"dc-otto-2-1": "02744b1757fcc192ad0819f0dbaed508.png",
		"dc-vpn-neu-0": "bd00de1db3073a627816a2ceac778267.png",
		"dc-vpn-neu-1": "ad2bdb5184a33f0c6f8b33f926c19bfb.png",
		"hpc-claix-2": "616cee53f5fd71f1a821b116b16a6234.png",
		"sc-lssnord-0": "6c63d3bae57553638f17a0f8ac9eb2cf.png",
		"sc-lssnord-1": "2ea8abaa49a908d625971051a76da3b2.png",
		"sc-sw23-0": "13e62f4d11030cc88447e8753d8b658a.png",
		"sc-sw23-1": "e34a01df9e0693ebb1b4324965bb7c0b.png",
	}
	AREAS = {
		"title": (30, 5, 580, 25),
		"incur": (190, 166, 265, 189),
		"inavg": (325, 166, 405, 189),
		"inmax": (470, 166, 550, 189),
		"outcur": (190, 196, 265, 219),
		"outavg": (325, 196, 405, 219),
		"outmax": (470, 196, 550, 219),
	}
	TRANSLATE = {
		"port-channell": "port-channel1",
		"n7k-wwlO-": "n7k-ww10-",
		"n7k-wwl0-": "n7k-ww10-",
		"n7k-wwlO0-": "n7k-ww10-",
		"c4k-wwl10": "c4k-ww10",
		"c4k-wwl0": "c4k-ww10",
		"c\u00e9k": "c4k",
		"c6\u00e9k": "c6k",
		"rx200-h00l": "rx200-h001",
		"Lssnord": "lssnord",
		"n7k-wwl10": "n7k-ww10"
	}

	def __init__(self):
		super().__init__()
		self.lastmodified = self._get_lastmodified()

	def sync(self):
		while True:
			lm = self._get_lastmodified()
			if lm != self.lastmodified:
				self.lastmodified = lm
				break
			time.sleep(10)

	def process_images(self):
		for img in self.image:
			self.image[img].process()
