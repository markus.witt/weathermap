from .weathermap import Weathermap
import time

class AMSIX(Weathermap):
	LIMITDOWNLOAD = 1
	BASEURL = "https://stats.ams-ix.net/"
	IMGURL = {
		"amsix": "cgi-bin/stats/16all?imgformat=png;log=totalall;png=daily"
	}
	AREAS = {
		"inavg": (100, 250, 210, 268),
		"inmax": (100, 235, 210, 255),
		"incur": (100, 265, 210, 285),
		"outavg": (305, 250, 410, 268),
		"outmax": (305, 235, 410, 255),
		"outcur": (305, 265, 410, 285)
	}
	TRANSLATE = {
		"Tbh/s": "Tb/s",
		",": "." # should be the last one
	}

	def __init__(self):
		super().__init__()
		self.syncts = 0

	def sync(self):
		time.sleep(max(0, self.syncts + 300 - int(time.time())))
		self.syncts = int(time.time())

	def process_images(self):
		for img in self.image:
			self.image[img].resize()
			self.image[img].sharpen()
			self.image[img].sharpen()
