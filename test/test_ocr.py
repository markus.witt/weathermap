import unittest
from weathermap.ocr import OCR
from weathermap.image import Image
import warnings

class OCRTest(unittest.TestCase):
	area = {
		"title": (30, 5, 580, 25),
		"incur": (190, 166, 265, 189),
		"inavg": (325, 166, 405, 189),
		"inmax": (470, 166, 550, 189),
		"outcur": (190, 196, 265, 219),
		"outavg": (325, 196, 405, 219),
		"outmax": (470, 196, 550, 219)
	}

	testimage = "test/data/xwin-dfn-fra.png"
	testdata = {'title': 'n7k-lssnord-1-xwin - Traffic - port-channell - DFN-FRA', 'incur': '4.86 G', 'inavg': '4.25 G', 'inmax': '8.27 G', 'outcur': '6.65 G', 'outavg': '6.84 G', 'outmax': '16.24 G'}

	def assertAlmostEqual(self, a, b, length=0, times=0, msg=None):
		delta_length = abs(len(a) - len(b))
		if delta_length > length:
			standardMsg = "length difference {} is greater than {}".format(delta_length, length)
			self.fail(self._formatMessage(msg, standardMsg))

		delta_times = 0
		for i in range(len(a)):
			if a[i] != b[i]:
				delta_times += 1
		if delta_times > times:
			standardMsg = "content differs {} times, which is greater than {}".format(delta_times, times)
			self.fail(self._formatMessage(msg, standardMsg))

	def setUp(self):
		self.image = Image(self.testimage, self.testimage)
		self.image.process()
		self.ocr = OCR(self.image, self.area)
		warnings.simplefilter("ignore", category=ResourceWarning)

	def tearDown(self):
		self.image.image.close()

	def test_ocr(self):
		ocr = self.ocr.ocr()
		for part in ocr:
			self.assertAlmostEqual(ocr[part], self.testdata[part], times=1)

	def test_single_ocr(self):
		for area in self.area:
			image = self.image.crop(self.area[area])
			data = self.testdata[area]
			self.assertAlmostEqual(self.ocr._ocr(image), data, times=1)
