from .weathermap import Weathermap
import time

class DECIX(Weathermap):
	LIMITDOWNLOAD = 2
	BASEURL = "https://www.de-cix.net/"
	IMGURL = {
		#"decix-fra": "traffic_FRA-2days-1170-400.png",
		"decix-ham": "traffic_HAM-2days-1170-400.png",
		"decix-muc": "traffic_MUC-2days-1170-400.png",
		"decix-dus": "traffic_DUS-2days-1170-400.png",
		"decix-nyc": "traffic_NYC-2days-1170-400.png",
		"decix-dfw": "traffic_DFW-2days-1170-400.png",
		"decix-mrs": "traffic_MRS-2days-1170-400.png",
		"decix-mad": "traffic_MAD-2days-1170-400.png",
		"decix-ist": "traffic_IST-2days-1170-400.png",
		"decix-dxb": "traffic_DXB-2days-1170-400.png",
		"decix-pmo": "traffic_PMO-2days-1170-400.png",
	}
	AREAS = {
		"title": (400, 0, 850, 25),
		"inavg": (140, 465, 215, 480),
		"inmax": (140, 480, 215, 495),
		"incur": (140, 492, 215, 507),
		"outavg": (425, 465, 495, 480),
		"outmax": (425, 480, 495, 495),
		"outcur": (425, 492, 495, 507)
	}
	TRANSLATE = {
		"\n": "",
		" G6": "G",
		" 6G": "G",
		" G": "G",
		"UAE-1X": "UAE-IX",
		",": "." # should be the last one
	}

	def __init__(self):
		super().__init__()
		self.syncts = 0

	def sync(self):
		time.sleep(max(0, self.syncts + 300 - int(time.time())))
		self.syncts = int(time.time())

	def process_images(self):
		for img in self.image:
			self.image[img].resize()
			self.image[img].sharpen()
			self.image[img].sharpen()
