# weathermap
Scrapes the [RWTH weathermap](https://netstatus.rz.rwth-aachen.de/status/weathermap/)
and imports the data into a graphite database.

## current process
0. Sync to the 5 minute interval
1. Download the weathermap images
2. Extract the data via tesseract
3. Output the data to a json file
