import unittest
import weathermap.download

class DownloaderTest(unittest.TestCase):
	url = ""
	useragent = "DownloaderTest Unit Test Test Case"

	def setUp(self):
		self.downloader = weathermap.download.Downloader(self.useragent)

	def tearDown(self):
		pass

	def test_useragent(self):
		full_ua = "{}; {}".format(self.useragent, self.downloader.CONTACT)
		self.assertEqual(self.downloader.session.headers["User-Agent"], full_ua)

	@unittest.skip("not implemented yet")
	def test_get(self):
		pass

	@unittest.skip("not implemented yet")
	def test_head(self):
		pass
