#!/usr/bin/env python
# runs a file through tesseract and returns the important stuff
import tesserocr

class OCRException(Exception):
	pass

class OCR:
	def __init__(self, image, areas):
		self.image = image
		self.areas = areas

	def _ocr(self, image, builder=None):
		ocr_result = tesserocr.image_to_text(image).strip()

		return ocr_result

	def ocr(self):
		rtn = {}

		for area in self.areas:
			cropped_image = self.image.crop(self.areas[area])
			rtn[area] = self._ocr(cropped_image)

		return rtn
