from .download import Downloader
from .ocr import OCR
from .image import Image

import time
import os
from multiprocessing import Pool
import functools

def retry(retries):
	def retry_decorator(func):
		@functools.wraps(func)
		def retry_wrapper(*args, **kwargs):
			counter = 0
			while counter <= retries:
				try:
					return func(*args, **kwargs)
				except OSError:
					counter += 1
					continue
			raise WeathermapException("Can't process {}".format(args[1]))
		return retry_wrapper
	return retry_decorator

class WeathermapException(Exception):
	pass

class Weathermap:
	GENERATOR = "weathermap scraper/1.3.0"
	LIMITDOWNLOAD = 8
	LIMITOCR = int(os.cpu_count()/2)

	def __init__(self):
		self.download = Downloader(self.GENERATOR)
		self._clear()

	def _clear(self):
		self.image = {}
		self.data = {}

	def _get_lastmodified(self, url=None):
		if url == None:
			url = self.BASEURL
		head = self.download.head(url)
		return head["Last-Modified"]

	@retry(2)
	def _get_image(self, name):
		data = self.download.get("{}{}".format(self.BASEURL, self.IMGURL[name]))
		img = Image(data, name)
		return {name: img}

	def _ocr(self, name):
		image = self.image[name]
		self.ocr = OCR(image, self.AREAS)
		content = self.ocr.ocr()

		return {name: content}

	def _fix_data(self):
		for con in self.data:
			for elem in self.data[con]:
				for translation in self.TRANSLATE:
					self.data[con][elem] = self.data[con][elem].replace(translation, self.TRANSLATE[translation])

	def _add_metadata(self):
		self.data["time"] = int(time.time())
		self.data["generator"] = self.GENERATOR

	def _parallelize(self, function, arguments, limit):
		rtn = {}

		with Pool(limit) as p:
			everything = p.map(function, arguments)
			p.close()
			p.join()
		for data in everything:
			rtn.update(data)

		return rtn

	def sync(self):
		raise NotImplementedError

	def process_images(self):
		raise NotImplementedError

	def _get(self):
		self._clear()

		self.image = self._parallelize(self._get_image, self.IMGURL, self.LIMITDOWNLOAD)
		self.process_images()
		self.data = self._parallelize(self._ocr, self.image, self.LIMITOCR)

		self._fix_data()

		self._add_metadata()

		return self.data

	def get(self):
		return self._get()
