import PIL.Image
import PIL.ImageDraw
import PIL.ImageFilter

class Image:
	RESIZEFACTOR = 3
	RESIZEMETHOD = PIL.Image.LANCZOS

	def get_image(self):
		return self.image

	def __init__(self, image, name):
		self.image = PIL.Image.open(image)
		self.image.filename = name


	def crop(self, area):
		box = tuple([elem * self.RESIZEFACTOR for elem in area])
		return self.image.crop(box=box)

	def process(self):
		self.threshold()
		self.resize()
		self.sharpen()

	def threshold(self):
		def threshold(arg):
			if arg > 235:
				return 255
			else:
				return arg
		self.image = self.image.point(threshold)

	def sharpen(self):
		self.image = self.image.filter(PIL.ImageFilter.SHARPEN)

	def resize(self):
		self.image = self.image.resize(
			(self.image.width * self.RESIZEFACTOR, self.image.height * self.RESIZEFACTOR),
			resample = self.RESIZEMETHOD
		)
